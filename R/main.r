#' @import ggplot2 pgscales plyr dplyr
#' @export
mapData = function(mappings, data, szScale = list(from = c(0,2), to = c(0, 50)),
                                              clScale = list(low = -1, mid = 0, high = 1),
                                              clValues = c("#3bef3b", "#000000", "#f42411") ){
  if(class(mappings) == "formula"){
    fterms = terms(mappings)
    mappings = rownames(attr(fterms, "factors"))
  }
  df = data.frame(names = data[[mappings[1]]], clrdata = data[[mappings[2]]], szdata = data[[mappings[3]]])
  df$size = pgscales::rescale(df$szdata, from = szScale$from, to = szScale$to, clip = TRUE)
  df$color = mapToDivColorMap(df$clrdata, clScale, clValues)
  return(df)
}

#' @export
kinhubParse = function(mappings, data, szScale =list(from = c(0,2), to = c(0, 50)),
                       clScale = list(low = -1, mid = 0, high = 1),
                       clValues = c("#3bef3b", "#000000", "#f42411") ){

    df = mapData(mappings, data, szScale, clScale, clValues)
    parsed_str = dlply(df,~names, .fun = function(x){
      crgb = col2rgb(x$color)
      str= c(paste("@0:",x$size,":",crgb[1],",",crgb[2],",",crgb[3])
                                  ,as.character(x$names))
    })
    return(parsed_str)
}

#' @export
treeFile = function(fname, mappings, data, parsefun = kinhubParse, ...){
    pstr = parsefun(mappings, data, ...)
    fid = file(description = fname, open = "wt")
    lapply(pstr, FUN = writeLines, con = fid)
    close(fid)
}

#' @export
uniprot2xname = function(id){
  mkmi = left_join(data.frame(UniprotID = id), kmindex, by = "UniprotID")
  return(mkmi)
}

