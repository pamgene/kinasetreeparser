library(ggplot2)
library(pgscales)
library(plyr)

s = kinhubParse(mappings = names ~ stat + score, data = test.data)

xname = uniprot2xname(example.data$Kinase_UniprotID)
treeFile("test.txt", mappings = Kinase_Name ~ meanStat + meanFeatScore, data = example.data)
